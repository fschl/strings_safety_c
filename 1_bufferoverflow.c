#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
    char owner[] = "fschl";
    int balance = 100;
    char note[10] = "leer";
    int amount = 0;

    printf("\n var \t size \t value \n");
    printf("bal \t %i \t %i \n", sizeof(balance), balance);
    printf("note \t %i \t %s \n", sizeof(note), note);
    printf("amount \t %i \t %i \n", sizeof(amount), amount);

    printf("Hallo %s, dein aktueller Kontostand ist: %d \n", owner, balance);
    printf("Bitte Überweisungsbetrag angeben: ");
    scanf("%d", &amount);
    
    char input[20];
    printf("Überweisungsgrund: ");
    scanf("%s", input);
    strcpy(note, input);

    balance -= amount;
    printf("Du hast %d überwiesen. Notiz:\n %s\n\n", amount, note);
    printf("Dein neuer Kontostand ist: %d\n", balance);
    
    printf("\naddr of bala \t %p \n", &balance);
    printf("addr of note \t %p \n", &note);
    printf("addr of amount\t %p \n", &amount);

    printf("\n var \t size \t value \n");
    printf("bala \t %i \t %i \n", sizeof(balance), balance);
    printf("note \t %i \t %s \n", sizeof(note), note);
    printf("amount \t %i \t %i \n", sizeof(amount), amount);

    printf("Bye, %s\n", owner);
    return 0;
}
