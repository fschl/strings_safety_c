/*
 * einfacher Bufferoverflow
 * Compiler (gcc) warnt vor Benutzung von gets()
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
  char command[] = "date";
  char name [8];

  printf("Hi, sag mir deinen Name und ich sage dir die Uhrzeit!\nName: ");
  char input[20];
  gets(input);

  strcpy(name, input);
  printf("\nDanke sehr %s, hier ist die Zeit:", name);
  printf("\n");

  system(command);

  printf("\nBis bald, %s!\n", name);

  return 0;
}
