#include<stdio.h>

int numberOfCharsInArray(char* array){
  int c = 0;
 
  while( *(array + c) != '\0' ) {
    c++;
  }
 
  return c;
}

int main() {
  int i;

  char *arr[] = {"C","C++","Java","golang"};
  char **ptr[] = &arr;

  for(i = 0; i < 4; i++) {
    printf("Address of %i : %p (%s) \t size: %d\n",
           i+1,
           &(arr[i]),
           (*ptr)[i],
           numberOfCharsInArray(arr[i]));
  }

  return 0;
}
