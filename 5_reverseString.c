#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// String manipulation mit Pointern
char * reverse_pointer(char * pString) {
  if (NULL == pString)
    return NULL;

  char *pStart = pString;
  char *pEnd = pStart + strlen(pString) - 1;

  while (pEnd > pStart) {
    char temp = *pStart;
    *pStart = *pEnd;
    *pEnd = temp;

    pStart++;
    pEnd--;
  }

  return pString;
}

int main() {
  char strin[]="Pointers in C are pretty fast!";
  char strout[]="!tsaf ytterp era C ni sretnioP";

  printf("input: %s \n", strin);

  reverse_pointer(strin);

  printf("real output: %s \n", strin);

  return 0;

}
