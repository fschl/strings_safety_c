#include <stdio.h>
 
int main() {
	// declare int ival and int pointer iptr.  Assign address of ival to iptr.
	int ival = 1;
	int *iptr = &ival;
 
	// dereference iptr to get value pointed to, ival, which is 1
	int get = *iptr;
	printf("*iptr = %d\n", get);
	printf("iptr = %p\n", iptr);

    char *thing;
    thing = iptr;
	printf("thing = %p\n", thing);

	// dereference iptr to set value pointed to, changes ival to 2
	*iptr = 5000;
	int set = *iptr;
	printf("*iptr = %d\n", set);
	printf("ival = %d\n", ival);

    *(thing) = 'a';
    *(thing+1) = 'b';
    printf("thing: %c\n", *thing);
	printf("ival = %d\n", ival);
	printf("ival as char = %c\n", ival);

    return 0;
}
