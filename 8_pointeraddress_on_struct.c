#include <stdio.h>
#include <stddef.h>

typedef struct{
    int a, b;
    double c, d;
} abcd_s;

int main() {

  abcd_s list[3];
  list[0].a = 1;  list[0].b = 2; list[0].c = 3.7;  list[0].d = 4.8;
  list[1].a = 5;  list[1].b = 6; list[1].c = 7;    list[1].d = 7.6;
  list[2].a = 8;  list[2].b = 8; list[2].c = 8.4;  list[2].d = 9.4; 
  printf("a of 0: %d \t at %p\n", list[0].a, &(list[0].a));
  printf("b of 1: %d \t at %p\n", list[1].b, &(list[1].b));

  int *pint;
  pint =  &list[0].a;
  printf("value of pint: %d \t at %p\n", *pint, pint);

  pint++;
  printf("\nvalue of pint++: %d \t at %p\n", *pint, pint);
  printf("value of list[0].b: %d \t at %p\n", list[0].b, &list[0].b);

  double *pdouble;
  pdouble = &list[0].c;
  printf("\nvalue of pdouble: %d \t at %p (list[1].c)\n", *pdouble, pdouble);
  
  pint++;
  pdouble = pint;
  printf("\nvalue of pint %d \t at %p\n", *pint, pint);

  return 0;
}
